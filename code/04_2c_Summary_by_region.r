#///////
# Summaries for Supplemental tables
# //////

# ---- SUMMARIZE _REGION_2c 
rm(list=ls())

#---- Load data and initial variables 
wdpng = "~/R/Pest-MS/png"
wdtables = "~/R/Pest-MS/tables"
wdfun = "~/R/Pest-MS/fun"
wdrdata = "~/R/Pest-MS/RData/"

# Load data
setwd(wdrdata)
load("ALL_2c.RData") 

# ---- SUM_DAT_REGION_2c (median, mean, range) ----
library(plyr) 
# summarize by "region","crop","phi", "fact" = serves for a general purpose and ggplot
SUM_DAT_REGION_2c <- ddply(DAT_2c, c("fact","region","phi","crop"), summarise,
                    median= median(value,na.rm = T),
                    mean= mean(value,na.rm = T),
                    min= min(value,na.rm = T),
                    max= max(value,na.rm = T))

# Save data
write.csv(SUM_DAT_REGION_2c, file.path(wdtables,"SUM_DAT_REGION_2c.csv"),row.names=F)
# save.image(file.path(wdrdata,"ALL_REGION_2c.RData"))



# ---- Table 1 IPM_REGION_2c: Split data by crops + IPM ----
library(dplyr)

# Extract
SUM_IPM_REGION_2c = SUM_DAT_REGION_2c[SUM_DAT_REGION_2c$fact=="IPM",]
# Remove unused
SUM_IPM_REGION_2c = SUM_IPM_REGION_2c[,-which(colnames(SUM_IPM_REGION_2c) %in% c("fact"),arr.ind = T)]
rownames(SUM_IPM_REGION_2c) = NULL

# Covert from long to wide
library(reshape2)
SUM_IPM_REGION_2c = reshape(SUM_IPM_REGION_2c, direction="wide", idvar=c("region","crop"), timevar="phi")

#extract crop
IPM_REGION_2c_MAIZE = SUM_IPM_REGION_2c[SUM_IPM_REGION_2c$crop=="M",]
IPM_REGION_2c_RICE = SUM_IPM_REGION_2c[SUM_IPM_REGION_2c$crop=="R",]
IPM_REGION_2c_WHEAT = SUM_IPM_REGION_2c[SUM_IPM_REGION_2c$crop=="W",]

# remove crop col
IPM_REGION_2c_MAIZE = dplyr::select(IPM_REGION_2c_MAIZE,-crop)
IPM_REGION_2c_RICE = dplyr::select(IPM_REGION_2c_RICE,-crop)
IPM_REGION_2c_WHEAT = dplyr::select(IPM_REGION_2c_WHEAT,-crop)

# horizontal re-ordering (columns)
cols = c("region","median.2","median.3","median.4","mean.2","mean.3","mean.4","min.2","max.2","min.3","max.3","min.4","max.4")
IPM_REGION_2c_MAIZE = IPM_REGION_2c_MAIZE[,cols]
IPM_REGION_2c_RICE = IPM_REGION_2c_RICE[,cols]
IPM_REGION_2c_WHEAT = IPM_REGION_2c_WHEAT[,cols]

# moving to % and rounding tables before export and concatination
IPM_REGION_2c_MAIZE[,-1] <- round(IPM_REGION_2c_MAIZE[,-1]*100,1)
IPM_REGION_2c_RICE[,-1] <- round(IPM_REGION_2c_RICE[,-1]*100,1)
IPM_REGION_2c_WHEAT[,-1] <- round(IPM_REGION_2c_WHEAT[,-1]*100,1)


# concatenate min,max columns
IPM_REGION_2c_MAIZE$range.2 =paste(IPM_REGION_2c_MAIZE$min.2,IPM_REGION_2c_MAIZE$max.2,sep=" to ")
IPM_REGION_2c_MAIZE$range.3 =paste(IPM_REGION_2c_MAIZE$min.3,IPM_REGION_2c_MAIZE$max.3,sep=" to ")
IPM_REGION_2c_MAIZE$range.4 =paste(IPM_REGION_2c_MAIZE$min.4,IPM_REGION_2c_MAIZE$max.4,sep=" to ")

IPM_REGION_2c_RICE$range.2 =paste(IPM_REGION_2c_RICE$min.2,IPM_REGION_2c_RICE$max.2,sep=" to ")
IPM_REGION_2c_RICE$range.3 =paste(IPM_REGION_2c_RICE$min.3,IPM_REGION_2c_RICE$max.3,sep=" to ")
IPM_REGION_2c_RICE$range.4 =paste(IPM_REGION_2c_RICE$min.4,IPM_REGION_2c_RICE$max.4,sep=" to ")

IPM_REGION_2c_WHEAT$range.2 =paste(IPM_REGION_2c_WHEAT$min.2,IPM_REGION_2c_WHEAT$max.2,sep=" to ")
IPM_REGION_2c_WHEAT$range.3 =paste(IPM_REGION_2c_WHEAT$min.3,IPM_REGION_2c_WHEAT$max.3,sep=" to ")
IPM_REGION_2c_WHEAT$range.4 =paste(IPM_REGION_2c_WHEAT$min.4,IPM_REGION_2c_WHEAT$max.4,sep=" to ")

# remove min,max columns
rem =  c("min.2","max.2","min.3","max.3","min.4","max.4")
IPM_REGION_2c_MAIZE = dplyr::select(IPM_REGION_2c_MAIZE,-one_of(rem))
IPM_REGION_2c_RICE = dplyr::select(IPM_REGION_2c_RICE,-one_of(rem))
IPM_REGION_2c_WHEAT = dplyr::select(IPM_REGION_2c_WHEAT,-one_of(rem))
rm(rem)


# vertical order by MEDIAN phi 3
IPM_REGION_2c_MAIZE = IPM_REGION_2c_MAIZE[order(IPM_REGION_2c_MAIZE$median.3,decreasing = T),]
IPM_REGION_2c_RICE = IPM_REGION_2c_RICE[order(IPM_REGION_2c_RICE$median.3,decreasing = T),]
IPM_REGION_2c_WHEAT = IPM_REGION_2c_WHEAT[order(IPM_REGION_2c_WHEAT$median.3,decreasing = T),]

write.csv(IPM_REGION_2c_MAIZE,file.path(wdtables,"ST1_IPM_REGION_2c_MAIZE.csv"),row.names=F)
write.csv(IPM_REGION_2c_RICE, file.path(wdtables,"ST1_IPM_REGION_2c_RICE.csv"),row.names=F)
write.csv(IPM_REGION_2c_WHEAT, file.path(wdtables,"ST1_IPM_REGION_2c_WHEAT.csv"),row.names=F)

rm(IPM_REGION_2c_MAIZE)
rm(IPM_REGION_2c_RICE)
rm(IPM_REGION_2c_WHEAT)


# ---- Table 2 TONNES_REGION_2c ----
# Sup 2:  Total yield and projected annual yield loss in 2050, across a full range of insect life-histories 

library(dplyr)

# TOTAL SUM _REGION_2c by "region","crop","phi"
SUM_TONNES_REGION_2c <- ddply(TONNES_2c, c("region","phi","crop"), summarise,
                    sum= sum(value,na.rm = T))
                    

# summarize PRESENT by "region","crop",
SUM_TONNES_REGION_PRES <- ddply(TONNES_PRES, c("region","crop"), summarise,
                       sum= sum(value,na.rm = T))

# Covert from long to wide
library(reshape2)
SUM_TONNES_REGION_2c = reshape(SUM_TONNES_REGION_2c, direction="wide", idvar=c("region","crop"), timevar="phi")

#extract crop
TONNES_REGION_2c_MAIZE = SUM_TONNES_REGION_2c[SUM_TONNES_REGION_2c$crop=="M",]
TONNES_REGION_2c_RICE = SUM_TONNES_REGION_2c[SUM_TONNES_REGION_2c$crop=="R",]
TONNES_REGION_2c_WHEAT = SUM_TONNES_REGION_2c[SUM_TONNES_REGION_2c$crop=="W",]

TONNES_REGION_PRES_MAIZE = SUM_TONNES_REGION_PRES[SUM_TONNES_REGION_PRES$crop=="M",]
TONNES_REGION_PRES_RICE = SUM_TONNES_REGION_PRES[SUM_TONNES_REGION_PRES$crop=="R",]
TONNES_REGION_PRES_WHEAT = SUM_TONNES_REGION_PRES[SUM_TONNES_REGION_PRES$crop=="W",]

# remove crop col
TONNES_REGION_2c_MAIZE = TONNES_REGION_2c_MAIZE[,-which(colnames(TONNES_REGION_2c_MAIZE) %in% "crop", arr.ind=T)]
TONNES_REGION_2c_RICE = TONNES_REGION_2c_RICE[,-which(colnames(TONNES_REGION_2c_RICE) %in% "crop", arr.ind=T)]
TONNES_REGION_2c_WHEAT = TONNES_REGION_2c_WHEAT[,-which(colnames(TONNES_REGION_2c_WHEAT) %in% "crop", arr.ind=T)]

# add PRES values _REGION_2c
TONNES_REGION_2c_MAIZE$present =TONNES_REGION_PRES_MAIZE$sum
TONNES_REGION_2c_RICE$present =TONNES_REGION_PRES_RICE$sum
TONNES_REGION_2c_WHEAT$present =TONNES_REGION_PRES_WHEAT$sum

# vertical order by PRES 
TONNES_REGION_2c_MAIZE = TONNES_REGION_2c_MAIZE[order(TONNES_REGION_2c_MAIZE$present,decreasing = T),]
TONNES_REGION_2c_RICE = TONNES_REGION_2c_RICE[order(TONNES_REGION_2c_RICE$present,decreasing = T),]
TONNES_REGION_2c_WHEAT = TONNES_REGION_2c_WHEAT[order(TONNES_REGION_2c_WHEAT$present,decreasing = T),]

# horizontal re-ordering (columns)
cols = c("region","present","sum.2","sum.3","sum.4")
TONNES_REGION_2c_MAIZE = TONNES_REGION_2c_MAIZE[,cols]
TONNES_REGION_2c_RICE = TONNES_REGION_2c_RICE[,cols]
TONNES_REGION_2c_WHEAT = TONNES_REGION_2c_WHEAT[,cols]
rm(cols)

# rounding tables before export and concatination
TONNES_REGION_2c_MAIZE[,-1] <- round(TONNES_REGION_2c_MAIZE[,-1],0)
TONNES_REGION_2c_RICE[,-1] <- round(TONNES_REGION_2c_RICE[,-1],0)
TONNES_REGION_2c_WHEAT[,-1] <- round(TONNES_REGION_2c_WHEAT[,-1],0)

# writing tables
write.csv(TONNES_REGION_2c_MAIZE,file.path(wdtables,"ST2_TONNES_REGION_2c_MAIZE.csv"),row.names=F)
write.csv(TONNES_REGION_2c_RICE, file.path(wdtables,"ST2_TONNES_REGION_2c_RICE.csv"),row.names=F)
write.csv(TONNES_REGION_2c_WHEAT, file.path(wdtables,"ST2_TONNES_REGION_2c_WHEAT.csv"),row.names=F)

rm(TONNES_REGION_2c_MAIZE)
rm(TONNES_REGION_2c_RICE)
rm(TONNES_REGION_2c_WHEAT)
rm(TONNES_REGION_PRES_MAIZE)
rm(TONNES_REGION_PRES_RICE)
rm(TONNES_REGION_PRES_WHEAT)



# ---- Table 3 YLPH_REGION_2c ----
# Sup 3: Yield and yield loss (tones per ha) caused by climate change impacts on crop pests,  across a full range of insect life-histories 
library(dplyr)
SUM_YLPH_REGION_2c <- ddply(YLPH_2c, c("region","phi","crop"), summarise,
                     median= median(value,na.rm = T),
                     mean= mean(value,na.rm = T),
                     min= min(value,na.rm = T),
                     max= max(value,na.rm = T))


# summarize PRESENT by "region","crop",
SUM_YL_REGION_PRES <- ddply(YL_PRES, c("region","crop"), summarise,
                     median= median(value,na.rm = T),
                     mean= mean(value,na.rm = T),
                     min= min(value,na.rm = T),
                     max= max(value,na.rm = T))

# Covert from long to wide
library(reshape2)
SUM_YLPH_REGION_2c = reshape(SUM_YLPH_REGION_2c, direction="wide", idvar=c("region","crop"), timevar="phi")

#extract crop
YLPH_REGION_2c_MAIZE = SUM_YLPH_REGION_2c[SUM_YLPH_REGION_2c$crop=="M",]
YLPH_REGION_2c_RICE = SUM_YLPH_REGION_2c[SUM_YLPH_REGION_2c$crop=="R",]
YLPH_REGION_2c_WHEAT = SUM_YLPH_REGION_2c[SUM_YLPH_REGION_2c$crop=="W",]

YL_REGION_PRES_MAIZE = SUM_YL_REGION_PRES[SUM_YL_REGION_PRES$crop=="M",]
YL_REGION_PRES_RICE = SUM_YL_REGION_PRES[SUM_YL_REGION_PRES$crop=="R",]
YL_REGION_PRES_WHEAT = SUM_YL_REGION_PRES[SUM_YL_REGION_PRES$crop=="W",]

# remove crop col
YLPH_REGION_2c_MAIZE = dplyr::select(YLPH_REGION_2c_MAIZE,-crop)
YLPH_REGION_2c_RICE = dplyr::select(YLPH_REGION_2c_RICE,-crop)
YLPH_REGION_2c_WHEAT = dplyr::select(YLPH_REGION_2c_WHEAT,-crop)

YL_REGION_PRES_MAIZE = dplyr::select(YL_REGION_PRES_MAIZE,-crop)
YL_REGION_PRES_RICE = dplyr::select(YL_REGION_PRES_RICE,-crop)
YL_REGION_PRES_WHEAT = dplyr::select(YL_REGION_PRES_WHEAT,-crop)

# horizontal re-ordering (columns)
cols = c("region","median.2","median.3","median.4","mean.2","mean.3","mean.4","min.2","max.2","min.3","max.3","min.4","max.4")
YLPH_REGION_2c_MAIZE = YLPH_REGION_2c_MAIZE[,cols]
YLPH_REGION_2c_RICE = YLPH_REGION_2c_RICE[,cols]
YLPH_REGION_2c_WHEAT = YLPH_REGION_2c_WHEAT[,cols]
rm(cols)


# ADD PRESENT VALS TO YLPH
YLPH_REGION_2c_MAIZE = dplyr::right_join(YL_REGION_PRES_MAIZE,YLPH_REGION_2c_MAIZE,by = "region")
YLPH_REGION_2c_RICE = dplyr::right_join(YL_REGION_PRES_RICE,YLPH_REGION_2c_RICE,by = "region")
YLPH_REGION_2c_WHEAT = dplyr::right_join(YL_REGION_PRES_WHEAT,YLPH_REGION_2c_WHEAT,by = "region")

# rounding columns to two decimal palces 
YLPH_REGION_2c_MAIZE[,-1] = round(YLPH_REGION_2c_MAIZE[,-1],2)
YLPH_REGION_2c_RICE[,-1] = round(YLPH_REGION_2c_RICE[,-1],2)
YLPH_REGION_2c_WHEAT[,-1] = round(YLPH_REGION_2c_WHEAT[,-1],2)

# concatenate min,max columns
YLPH_REGION_2c_MAIZE$range =paste(YLPH_REGION_2c_MAIZE$min,YLPH_REGION_2c_MAIZE$max,sep=" to ")
YLPH_REGION_2c_MAIZE$range.2 =paste(YLPH_REGION_2c_MAIZE$min.2,YLPH_REGION_2c_MAIZE$max.2,sep=" to ")
YLPH_REGION_2c_MAIZE$range.3 =paste(YLPH_REGION_2c_MAIZE$min.3,YLPH_REGION_2c_MAIZE$max.3,sep=" to ")
YLPH_REGION_2c_MAIZE$range.4 =paste(YLPH_REGION_2c_MAIZE$min.4,YLPH_REGION_2c_MAIZE$max.4,sep=" to ")

YLPH_REGION_2c_RICE$range =paste(YLPH_REGION_2c_RICE$min,YLPH_REGION_2c_RICE$max,sep=" to ")
YLPH_REGION_2c_RICE$range.2 =paste(YLPH_REGION_2c_RICE$min.2,YLPH_REGION_2c_RICE$max.2,sep=" to ")
YLPH_REGION_2c_RICE$range.3 =paste(YLPH_REGION_2c_RICE$min.3,YLPH_REGION_2c_RICE$max.3,sep=" to ")
YLPH_REGION_2c_RICE$range.4 =paste(YLPH_REGION_2c_RICE$min.4,YLPH_REGION_2c_RICE$max.4,sep=" to ")

YLPH_REGION_2c_WHEAT$range =paste(YLPH_REGION_2c_WHEAT$min,YLPH_REGION_2c_WHEAT$max,sep=" to ")
YLPH_REGION_2c_WHEAT$range.2 =paste(YLPH_REGION_2c_WHEAT$min.2,YLPH_REGION_2c_WHEAT$max.2,sep=" to ")
YLPH_REGION_2c_WHEAT$range.3 =paste(YLPH_REGION_2c_WHEAT$min.3,YLPH_REGION_2c_WHEAT$max.3,sep=" to ")
YLPH_REGION_2c_WHEAT$range.4 =paste(YLPH_REGION_2c_WHEAT$min.4,YLPH_REGION_2c_WHEAT$max.4,sep=" to ")

# remove min,max cols (YLPH)
rem =  c("min","max","min.2","max.2","min.3","max.3","min.4","max.4")
YLPH_REGION_2c_MAIZE = dplyr::select(YLPH_REGION_2c_MAIZE,-one_of(rem))
YLPH_REGION_2c_RICE = dplyr::select(YLPH_REGION_2c_RICE,-one_of(rem))
YLPH_REGION_2c_WHEAT = dplyr::select(YLPH_REGION_2c_WHEAT,-one_of(rem))
rm(rem)

# arrange by median
YLPH_REGION_2c_MAIZE = dplyr::arrange(YLPH_REGION_2c_MAIZE, desc(median))
YLPH_REGION_2c_RICE = dplyr::arrange(YLPH_REGION_2c_RICE, desc(median))
YLPH_REGION_2c_WHEAT = dplyr::arrange(YLPH_REGION_2c_WHEAT, desc(median))

# arrange for table format 
YLPH_REGION_2c_MAIZE = select(YLPH_REGION_2c_MAIZE,region,median,mean,range,median.2,median.3,median.4,mean.2,mean.3,mean.4,range.2,range.3,range.4)
YLPH_REGION_2c_RICE = select(YLPH_REGION_2c_RICE,region,median,mean,range,median.2,median.3,median.4,mean.2,mean.3,mean.4,range.2,range.3,range.4)
YLPH_REGION_2c_WHEAT = select(YLPH_REGION_2c_WHEAT,region,median,mean,range,median.2,median.3,median.4,mean.2,mean.3,mean.4,range.2,range.3,range.4)


# write csv
write.csv(YLPH_REGION_2c_MAIZE,file.path(wdtables,"ST3_YLPH_REGION_2c_MAIZE.csv"),row.names=F)
write.csv(YLPH_REGION_2c_RICE, file.path(wdtables,"ST3_YLPH_REGION_2c_RICE.csv"),row.names=F)
write.csv(YLPH_REGION_2c_WHEAT, file.path(wdtables,"ST3_YLPH_REGION_2c_WHEAT.csv"),row.names=F)

# remove unused
rm(YLPH_REGION_2c_MAIZE)
rm(YLPH_REGION_2c_RICE)
rm(YLPH_REGION_2c_WHEAT)
rm(YL_REGION_PRES_MAIZE)
rm(YL_REGION_PRES_RICE)
rm(YL_REGION_PRES_WHEAT)



# ---- Table 4 IYCC_REGION_2c ----
# Table S4: Percent of crop lost due to climate change, across a full range of insect life-histories 
library(dplyr)
library(plyr)

SUM_IYCC_REGION_2c <- ddply(IYCC_2c, c("region","phi","crop"), summarise,
                     median= median(value,na.rm = T),
                     mean= mean(value,na.rm = T),
                     min= min(value,na.rm = T),
                     max= max(value,na.rm = T))

# Covert from long to wide
library(reshape2)
SUM_IYCC_REGION_2c = reshape(SUM_IYCC_REGION_2c, direction="wide", idvar=c("region","crop"), timevar="phi")

#extract crop
IYCC_REGION_2c_MAIZE = SUM_IYCC_REGION_2c[SUM_IYCC_REGION_2c$crop=="M",]
IYCC_REGION_2c_RICE = SUM_IYCC_REGION_2c[SUM_IYCC_REGION_2c$crop=="R",]
IYCC_REGION_2c_WHEAT = SUM_IYCC_REGION_2c[SUM_IYCC_REGION_2c$crop=="W",]

# remove crop col
IYCC_REGION_2c_MAIZE = dplyr::select(IYCC_REGION_2c_MAIZE,-crop)
IYCC_REGION_2c_RICE = dplyr::select(IYCC_REGION_2c_RICE,-crop)
IYCC_REGION_2c_WHEAT = dplyr::select(IYCC_REGION_2c_WHEAT,-crop)

# horizontal re-ordering (columns)
cols = c("region","median.2","median.3","median.4","mean.2","mean.3","mean.4","min.2","max.2","min.3","max.3","min.4","max.4")
IYCC_REGION_2c_MAIZE = IYCC_REGION_2c_MAIZE[,cols]
IYCC_REGION_2c_RICE = IYCC_REGION_2c_RICE[,cols]
IYCC_REGION_2c_WHEAT = IYCC_REGION_2c_WHEAT[,cols]
rm(cols)

# shifting to % and rounding
IYCC_REGION_2c_MAIZE[,2:7] = round(IYCC_REGION_2c_MAIZE[,2:7]*100,2)
IYCC_REGION_2c_MAIZE[,8:13] = round(IYCC_REGION_2c_MAIZE[,2:7]*100,1)
IYCC_REGION_2c_RICE[,2:7] = round(IYCC_REGION_2c_RICE[,2:7]*100,2)
IYCC_REGION_2c_RICE[,8:13] = round(IYCC_REGION_2c_RICE[,8:13]*100,1)
IYCC_REGION_2c_WHEAT[,2:7] = round(IYCC_REGION_2c_WHEAT[,2:7]*100,2)
IYCC_REGION_2c_WHEAT[,8:13] = round(IYCC_REGION_2c_WHEAT[,8:13]*100,1)


# concatenate min,max columns
IYCC_REGION_2c_MAIZE$range.2 =paste(IYCC_REGION_2c_MAIZE$min.2,IYCC_REGION_2c_MAIZE$max.2,sep=" to ")
IYCC_REGION_2c_MAIZE$range.3 =paste(IYCC_REGION_2c_MAIZE$min.3,IYCC_REGION_2c_MAIZE$max.3,sep=" to ")
IYCC_REGION_2c_MAIZE$range.4 =paste(IYCC_REGION_2c_MAIZE$min.4,IYCC_REGION_2c_MAIZE$max.4,sep=" to ")

IYCC_REGION_2c_RICE$range.2 =paste(IYCC_REGION_2c_RICE$min.2,IYCC_REGION_2c_RICE$max.2,sep=" to ")
IYCC_REGION_2c_RICE$range.3 =paste(IYCC_REGION_2c_RICE$min.3,IYCC_REGION_2c_RICE$max.3,sep=" to ")
IYCC_REGION_2c_RICE$range.4 =paste(IYCC_REGION_2c_RICE$min.4,IYCC_REGION_2c_RICE$max.4,sep=" to ")

IYCC_REGION_2c_WHEAT$range.2 =paste(IYCC_REGION_2c_WHEAT$min.2,IYCC_REGION_2c_WHEAT$max.2,sep=" to ")
IYCC_REGION_2c_WHEAT$range.3 =paste(IYCC_REGION_2c_WHEAT$min.3,IYCC_REGION_2c_WHEAT$max.3,sep=" to ")
IYCC_REGION_2c_WHEAT$range.4 =paste(IYCC_REGION_2c_WHEAT$min.4,IYCC_REGION_2c_WHEAT$max.4,sep=" to ")

# remove min,max columns
rem =  c("min.2","max.2","min.3","max.3","min.4","max.4")
IYCC_REGION_2c_MAIZE = dplyr::select(IYCC_REGION_2c_MAIZE,-one_of(rem))
IYCC_REGION_2c_RICE = dplyr::select(IYCC_REGION_2c_RICE,-one_of(rem))
IYCC_REGION_2c_WHEAT = dplyr::select(IYCC_REGION_2c_WHEAT,-one_of(rem))
rm(rem)

# vertical order by MEDIAN phi 3
IYCC_REGION_2c_MAIZE = IYCC_REGION_2c_MAIZE[order(IYCC_REGION_2c_MAIZE$median.3,decreasing = T),]
IYCC_REGION_2c_RICE = IYCC_REGION_2c_RICE[order(IYCC_REGION_2c_RICE$median.3,decreasing = T),]
IYCC_REGION_2c_WHEAT = IYCC_REGION_2c_WHEAT[order(IYCC_REGION_2c_WHEAT$median.3,decreasing = T),]

# write csv
write.csv(IYCC_REGION_2c_MAIZE,file.path(wdtables,"ST4_IYCC_REGION_2c_MAIZE.csv"),row.names=F)
write.csv(IYCC_REGION_2c_RICE, file.path(wdtables,"ST4_IYCC_REGION_2c_RICE.csv"),row.names=F)
write.csv(IYCC_REGION_2c_WHEAT, file.path(wdtables,"ST4_IYCC_REGION_2c_WHEAT.csv"),row.names=F)

# remove unused
rm(IYCC_REGION_2c_MAIZE)
rm(IYCC_REGION_2c_RICE)
rm(IYCC_REGION_2c_WHEAT)

#---- SAVE DATA 2c ----
save.image(file.path(wdrdata,"ALL_REGION_2c.RData"))

