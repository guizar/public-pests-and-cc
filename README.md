## Pest and climate change
(Tewksbury JJ et al. Unpublished work)

This project assessed pest pressure on major food crops  (maize, wheat and rice) by modelling insect population growth and changes in metabolism under climate change. I collaborated with data analysis and visualisations. 

- [Code](https://gitlab.com/guizar/public-pests-and-cc/tree/master/code) 
- [Charts and figures](https://gitlab.com/guizar/public-pests-and-cc/tree/master/png) 